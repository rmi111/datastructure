//
//  LinkedListIterative.h
//  DataStructure
//
//  Created by Md Aminuzzaman on 5/17/17.
//  Copyright © 2017 Md Aminuzzaman. All rights reserved.
//
#pragma once

#ifndef LinkedListIterative_h
#define LinkedListIterative_h

#include <cstdio>

#pragma once 

using std::swap;

template <typename T>
class LinkedList
{
    private:
        class Node
        {
            private:
                friend class LinkedList;
        
                T data;
        
                Node *next;
        
                Node() : next(nullptr) {}
        
                Node(const T& data) : data(data),next(nullptr) {}
        
                ~Node()
                {
                    std::cout<< "Deleting Node with data:" << data << std::endl;
                    
                    next = nullptr;
                }
            public:
                typedef T value_type;
        };
    
    
        Node   *first;
        Node   *last;
        size_t size;
    
        Node* createNode(const T &item)
        {
            Node *node = new Node;
            
            node->data = item;
            node->next = nullptr;
            
            return node;
        }
    
    public:
    
        class Iterator : public std::iterator<std::bidirectional_iterator_tag, T>
        {
            private:
                Node *node;
            
            public:
            
                typedef T value_type;
                typedef T& reference;
                typedef T* pointer;
                typedef std::bidirectional_iterator_tag iterator_category;
            
                friend class LinkedList;
                friend class ConstIterators;
            
                Iterator() : node(nullptr) {}
                
                Iterator(Node *node) : node(node)  {}
                
                Iterator(const Iterator& iterator) : node(iterator.node) {}
                
                Iterator& operator= (const Iterator& rhs)
                {
                    if( this != &rhs )
                    {
                        node = rhs.node;
                    }
                }
                
                Iterator& operator++()
                {
                    node = node->next;
                    
                    return *this;
                }
                
                Iterator& operator+(size_t index)
                {
                    while( index-- > 0 && ( node != nullptr ) )
                    {
                        ++this;
                    }
                    
                    return *this;
                }
           
                bool operator==(const Iterator &iter)
                {
                    return node == iter.node;
                }
            
                bool operator!=(const Iterator &iter)
                {
                    return node != iter.node;
                }
            
                reference operator* () const { return node->data; }
                pointer   operator->() const { return node; }
        };
    
        class ConstIterator : std::iterator<std::bidirectional_iterator_tag, const T>
        {
            private:
                const Node *node;
            public:
                friend class Iterator;
                friend class LinkedList;
            
                typedef T value_type;
                typedef const T& reference;
                typedef const T* pointer;
        
                ConstIterator() : node(nullptr) {}
        
                ConstIterator(const Node *node) : node(node)  {}
        
                ConstIterator(const Iterator& iterator) : node(iterator.node) {}
        
                const ConstIterator& operator= (const Iterator& rhs)
                {
                    if( this != &rhs )
                    {
                        node = rhs.node;
                    }
            
                    return *this;
                }
        
            const ConstIterator& operator++()
            {
                node = node->next;
            
                return *this;
            }
        
            const ConstIterator& operator+(size_t index)
            {
                while( index-- > 0 && ( node != nullptr ) )
                {
                    ++this;
                }
            
                return *this;
            }
        
            bool operator==(const ConstIterator &iter)
            {
                return node == iter.node;
            }
        
            bool operator!=(const ConstIterator &iter)
            {
                return node != iter.node;
            }
        
            reference operator*() const { return node->data; }
            pointer operator->()  const { return node; }
    };
    public:
        typedef Iterator iterator;
        typedef ConstIterator const_iterator;
        
        LinkedList() : size(0), first(nullptr), last(nullptr)
        {
            
        }
    
        LinkedList(const LinkedList& list) : size(0), first(nullptr), last(nullptr)
        {
            for( const_iterator iter = list.begin() ; iter != list.end(); ++ iter )
            {
                add(*iter);
            }
        }
    
        LinkedList& operator=(LinkedList<T> list)
        {
            std::swap(list.first, this->first);
            std::swap(list.last , this->last );
            std::swap(list.size , this->size );
            
            return *this;
        }
    
        // Add item at the end of the list
        void add(const T& item)
        {
            Node *node = createNode(item);
            
            if(first == nullptr)
            {
                first = node;
                last  = node;
            }
            else
            {
                last->next = node;
                last = node;
            }
            
            ++size;
        }
        
        void add(const T& item,size_t index)
        {
            if( size == 0 )
            {
                add(item);
            }
            else if( index == 0 && size > 0 )
            {
                addToFirst(item);
            }
            else if( index >= size )
            {
                addToLast(item);
            }
            else
            {
                Node *prev = first;
                Node *curr = first;
                
                size_t i = 0;
                
                while( i++ < index  )
                {
                    prev = curr;
                    curr = curr->next;
                }
                
                Node *node = createNode(item);
                
                prev->next = node;
                node->next = curr;
                
                ++size;
            }
        }
        
        void addToFirst(const T& item)
        {
            Node *node = createNode(item);
            
            node->next = first;
            
            first = node;
            
            if(size == 0)
                last = node;
            
            ++size;
        }
        
        void addToLast(const T& item)
        {
            Node *node = createNode(item);
            
            last->next = node;
            
            last = node;
            
            if(size == 0)
                first = node;
            
            ++size;
        }
        
        void removeFirst()
        {
            if( first == nullptr )
                return;
            
            Node *temp = first;
            
            first = first->next;
            
            --size;
            
            delete temp;
            
            if( size == 0 )
                last = first;
        }
        
        void remove(const size_t index)
        {
            if( size == 0 || index > size - 1 )
                throw std::out_of_range("Out of range");
            
            if(index == 0)
            {
                removeFirst();
                return;
            }
            
            Node *curr = first;
            Node *prev = first;
            
            size_t i(0);
            
            while( i++ < index )
            {
                prev = curr;
                curr = curr->next;
            }
            
            if( curr == last )
            {
                last = prev;
            }
            
            prev->next = curr->next;
            
            delete curr;
            
            --size;
        }
        
        void removeLast()
        {
            if( first == nullptr )
                return;
            
            Node *curr = first;
            Node *prev = first;
            
            while( curr != last )
            {
                prev = curr;
                curr = curr->next;
            }
            
            prev->next = nullptr;
            
            delete last;
            
            last = prev;
            
            --size;
            
            if( size == 0 )
                first = last;
        }
        
        T& getItem(size_t index) const
        {
            if(index > size)
                throw std::out_of_range("index out of bound!");
            
            Node *curr = first;
            
            size_t i = 0;
            
            while ( i++ != index )
                curr = curr->next;
            
            return curr->data;
        }
        
        size_t length() const
        {
            return size;
        }
        
        iterator begin()
        {
            return iterator(first);
        }
        
        iterator end()
        {
            return iterator(nullptr);
        }
        
        const_iterator begin() const
        {
            return const_iterator(first);
        }
        
        const_iterator end() const
        {
            return const_iterator(nullptr);
        }
    
        void clear()
        {
            Node *curr = first;
            
            while( curr != nullptr )
            {
                Node *temp = curr;
                
                curr = curr->next;
                
                delete temp;
                
                temp = nullptr;
                
                --size;
            }
            
            first = nullptr;
            last  = nullptr;
        }
        
        ~LinkedList()
        {
            clear();
        }
};


#endif 

/* LinkedListIterative_h */
