//
//  Stack.hpp
//  DataStructure
//
//  Created by Md Aminuzzaman on 6/25/16.
//  Copyright © 2016 Md Aminuzzaman. All rights reserved.
//
//  A class to hold fix capacity of stack.
//

#pragma once

#ifndef Stack_hpp
#define Stack_hpp

#include <cstdio>
#include <memory>

template <typename T>
class Stack
{
	private:

	class Iterator
	{
			T* data;

			friend class ConstIterator;
		public:
			Iterator(T* data):data(data)
			{
			
			}

			const T& operator*()
			{
				return *data;
			}

			Iterator& operator++()
			{
				++data;
				return *this;
			}

			Iterator& operator--()
			{
				--data;
				return *this;
			}

			Iterator operator++(int)
			{
				Iterator temp(data);

				++data;

				return temp;
			}

			Iterator operator--(int)
			{
				Iterator temp(data);

				--data;

				return temp;
			}

			bool operator!=(const Iterator& rhs)
			{
				return rhs.data != data;
			}
	};

	class ConstIterator
	{
			const T* data;

		public:
			
			ConstIterator(const T* data):data(data)
			{
			
			}

			ConstIterator(const Iterator& iter):data(iter.data)
			{
			
			}

			const T& operator*()
			{
				return *data;
			}

			ConstIterator& operator++()
			{
				++data;
				return *this;
			}

			ConstIterator& operator--()
			{
				--data;
				return *this;
			}

			ConstIterator operator++(int)
			{
				ConstIterator temp(data);

				++data;

				return temp;
			}

			ConstIterator operator--(int)
			{
				ConstIterator temp(data);

				--data;

				return temp;
			}

			bool operator!=(const ConstIterator& rhs)
			{
				return rhs.data != data;
			}
	};

	public:
		typedef T value_type ;
		typedef T* pointer_type ;
		typedef T& reference_type;

		typedef Iterator iterator;
		typedef ConstIterator const_iterator;

		iterator begin() 
		{
			return iterator(data);
		}
		
		iterator end() 
		{
			return iterator(data + index);
		}

		const_iterator begin() const
		{
			return const_iterator(data);
		}
		
		const_iterator end() const
		{
			return const_iterator(data + index);
		}
    private:
        T  *data;
        size_t size;
        size_t index;
        std::allocator<T> alloc;

		void resize(int nsize)
		{
			this->size = nsize;

			T *rblock = alloc.allocate(size);
			
			std::uninitialized_copy(data, data + index , rblock);

			data = rblock;
		}

	public:
	    Stack() : size (2) , index (0)
		{
			T t;

			data = alloc.allocate(size);
		}
		
        explicit Stack(size_t count) : size(count) , index(0)
        {
            data = alloc.allocate(size);
        }
    
        Stack(const Stack& stack)
        {
            size  = stack.size;
            index = stack.index;
            
            data = alloc.allocate(size);
            
            std::uninitialized_copy(stack.data,stack.data + size + 1, data);
        }
    
        Stack& operator=(Stack stack)
        {
			std::swap(this->size  , stack.size);
			std::swap(this->index , stack.index);
			std::swap(this->data  , stack.data);
            
            return *this;
        }
    
        void push(const T &t)
        {
            if(index == size)
            {
                resize(size * 2);
            }
        
			std::uninitialized_fill(data + index, data + index + 1, t);

			++ index;
        }
    
        T& pop()
        {
			if(index == 0)
				throw std::out_of_range("Index out of bound");

			if(index > 0 && index == (size / 4))
				resize(size / 2);
			
			--index;

            return data[index];
        }
    
        bool isEmpty() const
        {
            return index == 0;
        }

		
        ~Stack()
        {
            T *last = data + size;
            
            while(last != data)
                alloc.destroy(--last);
            
            alloc.deallocate(data, size);
            
            index = 0;
        }

		size_t getsize() const
		{
			return size;
		}
};

#endif 
/* Stack_hpp */
