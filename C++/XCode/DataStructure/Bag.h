//
//  Bag.h
//  DataStructure
//
//  Created by Md Aminuzzaman on 17/11/17.
//  Copyright © 2017 Md Aminuzzaman. All rights reserved.
//
#pragma once

#ifndef BAG_H
#define BAG_H

#include <cstdio>
#include <memory>
#include <iostream>

template <typename T>
class Bag
{
    private:
        class Node
        {
            public:
                T data;
                Node *next;
            
                Node(const T &t) : data(t), next(nullptr)
                {
                    
                }
        };
    
        Node *top;
        size_t size;
    public:
        Bag() : top(nullptr), size(0)
        {
            
        }
    
        void add(const T& t)
        {
            Node *curr = top;
            
            top = new Node(t);
            
            top->next = curr;
            
            ++size;
        }
    
        T pop()
        {
            if(size == 0)
                throw std::out_of_range("bag is empty");
            
            --size;
            
            Node *curr = top;
            
            T data(curr->data);
            
            top = top->next;
            
            delete curr;
            
            return data;
        }
    
        bool isEmpty() const
        {
            return size == 0;
        }
    
        ~Bag()
        {
            Node *curr = top;
            
            while( curr != nullptr )
            {
                Node *temp = curr;
                
                curr = curr->next;
                
                std::cout << "deleting " << temp->data << std::endl;
                
                delete temp;
            }
        }
};

#endif
