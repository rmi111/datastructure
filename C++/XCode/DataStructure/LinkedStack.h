//
//  LinkedStack.h
//  DataStructure
//
//  Created by Md Aminuzzaman on 6/30/16.
//  Copyright © 2016 Md Aminuzzaman. All rights reserved.
//
#pragma once

#ifndef LinkedStack_h
#define LinkedStack_h

#include <cstdio>

template<typename T>
class LinkedStack
{
    private:
        struct Node
        {
            T *item;
            Node *next;
        };

        Node<T> *top;
        size_t size;
    
        LinkedStack(const LinkedStack& rhs)
        {
        }
    
        LinkedStack& operator= (const LinkedStack& rhs)
        {
        }
    public:
        explicit LinkedStack() : top(nullptr),size(0)
        {
            
        }
    
        void push(T &item)
        {
            node<T> *old = top;
            
            node<T>* n = new node<T>;
            
            n->item = new T;
            
            *(n->item) = item;
            
            n->next = old;
            
            top = n;
            
            ++ size;
        }
    
        T pop()
        {
            node<T> *old = top;
            
            T item = *(old->item);
            
            top = top->next;
            
            delete old->item;
            delete old;
            
            --size;
            
            return item;
        }
    
        T& peek() const
        {
            if(top != nullptr)
                return *(top->item);
        }
    
        bool isEmpty() const
        {
            return top == nullptr;
        }
    
        ~LinkedStack()
        {
            while(top != nullptr)
                pop();
        }
};

#endif 
/* LinkedStack_h */
