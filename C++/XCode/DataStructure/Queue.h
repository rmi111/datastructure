//
//  Queue.h
//  DataStructure
//
//  Created by Md Aminuzzaman on 7/27/16.
//  Copyright © 2016 Md Aminuzzaman. All rights reserved.
//
#pragma once

#ifndef Queue_h
#define Queue_h

template <typename item>
class Queue
{
    private:
        item* data;
        size_t front;
        size_t rear;
        size_t size;
        size_t count;
        bool isEmpty;
    
        Queue(const Queue& queue){}

        Queue& operator=(const Queue& queue){}
    public:
        Queue(size_t size) : size(size),front(0),rear(0),count(0),data(new item[size])
        {
        }
    
        void enqueue(const item& it)
        {
            if(count == size - 1)
                return;
            
            data[count ++] = it;
        }
    
        item dequeue()
        {
            if(count == 0)
                return data[0];
            
            return data[count --];
        }
    
        ~Queue(){}
    
};

#endif /* Queue_h */
