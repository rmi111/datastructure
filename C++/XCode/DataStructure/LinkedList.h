//
//  LinkedList.h
//  DataStructure
//
//  Created by Md Aminuzzaman on 3/23/17.
//  Copyright © 2017 Md Aminuzzaman. All rights reserved.
//
#pragma once

#ifndef LinkedList_h
#define LinkedList_h

#include <cstdio>

template<typename T>
struct Node
{
    T data;
    Node *next;
};

template <typename T>
class LinkedList
{
    private:
        size_t size;
        Node<T> *first;
        Node<T> *last;
    
        Node<T>* createNode(const T &item)
        {
            Node<T> *node = new Node<T>;
            
            node->data = item;
            node->next = nullptr;
            
            return node;
        }
    
        LinkedList(const LinkedList& list){}
        LinkedList& operator=(const LinkedList& list){}
    public:
        LinkedList() : size(0), first(nullptr), last(nullptr)
        {
            
        }
    
        // Add item at the end of the list
        void add(T item)
        {
            Node<T> *node = createNode(item);
            
            if(first == nullptr)
            {
                first = node;
                last  = node;
            }
            else
            {
                last->next = node;
                last = node;
            }
            
            ++size;
        }
    
        void add(T item,size_t index)
        {
            if( size == 0 )
            {
                add(item);
            }
            else if( index == 0 && size > 0 )
            {
                addToFirst(item);
            }
            else if( index >= size )
            {
                addToLast(item);
            }
            else
            {
                Node<T> *prev = first;
                Node<T> *curr = first;
                
                int i = 0;
                
                while( i++ < index  )
                {
                    prev = curr;
                    curr = curr->next;
                }
                
                Node<T> *node = createNode(item);
                
                prev->next = node;
                node->next = curr;
                
                ++size;
            }
        }
    
        void addToFirst(T item)
        {
            Node<T> *node = createNode(item);
            
            node->next = first;
            
            first = node;
            
            if(size == 0)
                last = node;
            
            ++size;
        }
    
        void addToLast(T item)
        {
            Node<T> *node = createNode(item);
            
            last->next = node;
            
            last = node;
            
            if(size == 0)
                first = node;
            
            ++size;
        }
    
        void removeFirst()
        {
            if( first == nullptr )
                return;
            
            Node<T> *temp = first;
            
            first = first->next;
            
            --size;
            
            delete temp;
            
            if( size == 0 )
                last = first;
        }
    
        void remove(size_t index)
        {
            if( size == 0 || index > size - 1 )
                throw std::out_of_range("Out of range");
            
            if(index == 0)
            {
                removeFirst();
                return;
            }
            
            Node<T> *curr = first;
            Node<T> *prev = first;
            
            size_t i(0);
            
            while( i++ < index )
            {
                prev = curr;
                curr = curr->next;
            }
            
            if( curr == last )
            {
                last = prev;
            }
            
            prev->next = curr->next;
            
            delete curr;
            
            --size;
        }
    
        void removeLast()
        {
            if( first == nullptr )
                return;
            
            Node<T> *curr = first;
            Node<T> *prev = first;
            
            while( curr != last )
            {
                prev = curr;
                curr = curr->next;
            }
            
            prev->next = nullptr;
            
            delete last;
            
            last = prev;
            
            --size;
            
            if( size == 0 )
                first = last;
        }
    
        T& getItem(size_t index) const
        {
            if(index > size)
                throw std::out_of_range("index out of bound!");
            
            Node<T> *curr = first;
            
            size_t i = 0;
            
            while ( i++ != index )
                curr = curr->next;
            
            return curr->data;
        }
    
        size_t length() const
        {
            return size;
        }

        ~LinkedList()
        {
            Node<T> *curr = first;
            
            while( curr != last )
            {
                Node<T> *temp = curr;
                
                curr = curr->next;
                
                delete temp;
            }
        }
};

#endif 
/* LinkedList_h */
