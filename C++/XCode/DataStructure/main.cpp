//
//  main.cpp
//  DataStructure
//
//  Created by Md Aminuzzaman on 6/25/16.
//  Copyright © 2016 Md Aminuzzaman. All rights reserved.
//

#include <iostream>
#include "Bag.h"
#include "Stack.hpp"
#include "LinkedListIterative.h"

void printList(LinkedList<int>& list)
{
    for(LinkedList<int>::const_iterator iter = list.begin(); iter != list.end(); ++iter)
    {
        std::cout << *iter << " ";
    }
    
    std::cout << std::endl;
}

void testLinkList()
{
    LinkedList<int> list;
    
    list.add(1,0);
    
    std::cout << "add(1,0): "<< std::endl;
    
    printList(list);
    
    list.add(2,1);
    
    std::cout << "add(2,1): "<< std::endl;
    
    printList(list);
    
    list.add(3,2);
    
    std::cout << "add(3,2): "<< std::endl;
    
    printList(list);
    
    list.add(4,2);
    
    std::cout << "add(4,2): "<< std::endl;
    
    printList(list);
    
    std::cout << "Copy list test:" << std::endl;
    
    LinkedList<int> listCopy(list);
    
    printList(listCopy);
    
    std::cout << "Assign list test:" << std::endl;
    
    LinkedList<int> listAssign;
    
    listAssign.add(5);
    listAssign.add(6);
    listAssign.add(7);
    
    std::cout << "Before assign:" << std::endl;
    printList(listAssign);
    
    std::cout << "After assign:" << std::endl;
    listAssign = list;
    
    printList(listAssign);
}

void TestBag()
{
    Bag<int> bag;
    
    bag.add(1);
    bag.add(2);
    bag.add(3);
    bag.add(4);
    
    std::cout << bag.pop() << std::endl;
    std::cout << bag.pop() << std::endl;
    
    //while (!bag.isEmpty())
       // std::cout << bag.pop() << std::endl;
}


void TestStack()
{
	Stack<int> stack(10);

	stack.push(10);
	stack.push(20);
	stack.push(30);
	stack.push(40);
	stack.push(50);
	stack.push(70);

	std::cout << "Copied value of st into st2:" << std::endl;

	Stack<int> st2(stack);
	
	for(Stack<int>::const_iterator iter = st2.begin(); iter != st2.end(); ++iter)
	{
		std::cout << *iter << std::endl;
	}

	Stack<int> st3(3);

	st3.push(11);
	st3.push(12);
	st3.push(13);
	st3.push(14);

	std::cout << "st3:" << std::endl;

	for(Stack<int>::const_iterator iter = st3.begin(); iter != st3.end(); ++iter)
	{
		std::cout << *iter << std::endl;
	}

	st3.push(15);

	std::cout << "st2 = st3:" << std::endl;

	st2 = st3;

	for(Stack<int>::const_iterator iter = st3.begin(); iter != st3.end(); ++iter)
	{
		std::cout << *iter << std::endl;
	}

	std::cout << "st2:" << std::endl;

	for(Stack<int>::const_iterator iter = st2.begin(); iter != st2.end(); ++iter)
	{
		std::cout << *iter << std::endl;
	}

	try
	{
		std::cout << stack.pop() << " " << stack.getsize() << std::endl;
		std::cout << stack.pop() << " " << stack.getsize() << std::endl;
		std::cout << stack.pop() << " " << stack.getsize() << std::endl;
		std::cout << stack.pop() << " " << stack.getsize() << std::endl;
		std::cout << stack.pop() << " " << stack.getsize() << std::endl;
		std::cout << stack.pop() << " " << stack.getsize() << std::endl;
		std::cout << stack.pop() << " " << stack.getsize() << std::endl;
		std::cout << stack.pop() << " " << stack.getsize() << std::endl;
	}
	catch(std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
	//std::cout << stack.pop() << " " << stack.getsize() << std::endl;
	//std::cout << stack.pop() << " " << stack.getsize() << std::endl;
}

int main(int argc, const char * argv[])
{
    TestStack();
    
    return 0;
}

