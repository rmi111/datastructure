/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;

/**
 *
 * @author aaa111
 */
public class ThreeSumFast {
    public static int count(int[] a)
    {      
        Arrays.sort(a);
        
        int N = a.length;
        int cnt = 0;
        
        for(int i = 0 ; i < N; ++i)
        {
            for(int j = i ; j < N ; ++j)
                if(BinarySearch.rank( -a[i] - a[j], a) > j)
                    ++cnt;
        }
        
        return cnt;
    }
    
    public static void main(String[] args)
    {
        int N = Integer.parseInt("100000");
        
        int[] a = new int[N];
        
        for(int i = 0 ; i < N; ++i)
            a[i] = StdRandom.uniform(-1000000, 1000000);
        
        StopWatch watch = new StopWatch();
       
        StdOut.println(count(a));
        
        double time = watch.elapsedTime();
        
        StdOut.println(time + " seconds");
    }
}
