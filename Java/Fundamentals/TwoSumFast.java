/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;

/**
 *
 * @author aaa111
 */
public class TwoSumFast 
{
    public static int count(int[] a)
    {
        Arrays.sort(a);
        
        int N = a.length;
        
        int cnt = 0;
        
        for(int i = 0 ; i < N ; ++i)
        {
            if(BinarySearch.rank(-a[i], a) > i)
                ++cnt;
        }
        
        return cnt;
    }
    
    public static void main(String[] args)
    {
        int MAX = 1000000;
        int[] a = new int[MAX];
        
        for(int i = 0; i < MAX ; ++i)
            a[i] = StdRandom.uniform(-MAX,MAX);
        
        StdOut.println(count(a));
    }
    
}
