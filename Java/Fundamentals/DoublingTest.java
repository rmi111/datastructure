/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aaa111
 */
public class DoublingTest 
{
    public static double timeTrial(int N)
    {
        int MAX = 1000000;
        int[] a = new int[N];
        
        for(int i = 0; i < N ; ++i)
            a[i] = StdRandom.uniform(-MAX,MAX);
        
        StopWatch timer = new StopWatch();
        
        int count = ThreeSum.count(a);
        
        return timer.elapsedTime();
    }
    
    public static void main(String[] args)
    {
        for(int N = 250 ; true ; N+= N)
        {
            double time = timeTrial(N);
            StdOut.printf("%7d %5.1f\n",N,time);
        }
    }
}
