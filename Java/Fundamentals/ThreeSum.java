/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aaa111
 */
public class ThreeSum {
    public static int count(int[] a)
    {
        int N = a.length;
        
        int cnt = 0;
        
        for(int i = 0 ; i < N ; i++)
            for(int j = i+1 ; j < N; ++j)
                for(int k = j+1 ; k < N; ++k)
                    if(a[i] + a[j] + a[k] == 0)
                        cnt ++;
        
        return cnt;
    }
    
    public static void main(String[] args)
    {
        int N = Integer.parseInt("6000");
        
        int[] a = new int[N];
        
        for(int i = 0 ; i < N; ++i)
            a[i] = StdRandom.uniform(-1000000, 1000000);
        
        StopWatch watch = new StopWatch();
       
        StdOut.println(count(a));
        
        double time = watch.elapsedTime();
        
        StdOut.println(time + " seconds");
    }
}
