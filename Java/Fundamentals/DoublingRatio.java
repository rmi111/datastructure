/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aaa111
 */
public class DoublingRatio {
    
    public static void main(String[] args)
    {
        double prev = timeTrial(125);
        
        for(int N = 250 ; true; N += N)
        {
            double time = timeTrial(N);
            StdOut.printf("%6d %7.1f\n",N,time);
            StdOut.printf("%5.1f\n",time / prev);
            prev = time;
        }
    }
    
}
