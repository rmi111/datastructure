/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*Node class represents Node in Linked List.Holds generic data type 
  reference along with node reference to point the next node in the list

*/
public class Node<T> 
{
    public T item;
    public Node next;    
}
