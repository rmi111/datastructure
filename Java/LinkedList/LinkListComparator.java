public interface LinkListComparator<T> 
{
    public boolean compare(T compareTo,T compareWith);
}
