/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Iterator;

/**
 *
 * @author aaa111
 */
public class LinkedListTest
{
    public static void main(String[] args)
    {
        LinkedList<String> list = new LinkedList<>();
        
        list.add("A");
        list.add("A");
        list.add("B");
        list.add("E");
        list.add("A");
        list.add("C");
        list.add("D");
        list.add("A");
        list.add("A");
        list.add("E");
        
        //list.add("A");
        
        //list.removeLast();
        
        //list.delete(0);
            
        LinkListComparator<String> comparator = new LinkListComparator<String>()
        {
            @Override
            public boolean compare(String compareTo,String compareWith)
            {
                return compareTo.equals(compareWith);
            }
        };
        
        /*if(list.find("B", comparator))
        {
            System.out.println("Item Found");
        }*/
        
        Node first = new Node();
        
        first.item = "D";
        
        Node second = new Node();
        
        second.item = "E";
        
        //list.removeAfter(node, comparator);
        
        //list.insertAfter(first, second, comparator);
        
        list.remove("A", comparator);
        
        LinkListComparator<String> maxComparator = new LinkListComparator<String>()
        {
            @Override
            public boolean compare(String compareTo,String compareWith)
            {
                return (compareTo.compareTo(compareWith) > 0);
            }
        };
        
        String str = list.maxRecursive(maxComparator);
        
        System.out.println("Max:" + str);
        
        Iterator<String> it = list.iterator();
        
        while(it.hasNext())
        {
            String s = it.next();
            
            System.out.println(s);
        }
    }
}
