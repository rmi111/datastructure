

import java.util.Iterator;

/*
  A linked list class which hold a series of nodes,each node holds reference to its next 
  node in the list. The first item is referenced by the first node reference in Link List.
      
  Node-1               Node-2
  [next (Node-2)] ---> [next (null)] ---> null
  [item]               [item]
 */
public class LinkedList <T> implements Iterable<T>
{
    public int size;   
    private Node last;
    private Node first;
    
    /*private class Node<T> 
    {
        public T item;
        public Node next;    
    }*/
    
    public LinkedList()
    {
        size = 0;
    }
    
    public boolean isEmpty()
    {
        return first == null;
    }
    
    public void removeLast()
    {
        Node prev = first;
        Node curr = first;
        
        while(curr.next != null)
        {
            prev = curr;
            curr = curr.next;
        }
        
        prev.next = null;
        
        --size;
    }
    
    public void removeAfter(Node<T> node,LinkListComparator<T> comparator)
    {
        Node<T> curr = first;
        
        while(curr.next != null)
        {
            if(comparator.compare(node.item, curr.item))
            {
                if(curr.next != null)
                {
                    curr.next = curr.next.next;
                    --size;
                }
                
                break;
            }
             
            curr = curr.next;
        }
    }
    
    public T max(LinkListComparator<T> comparator)
    {
        Node curr = first;
        T max     = (T) curr.item;
                
        while(curr != null)
        {
            if(comparator.compare((T) curr.item, max))
                max = (T)curr.item;
            
            curr = curr.next;
        }
       
        return max;
    }
    
    public T maxRecursive(LinkListComparator<T> comparator)
    {
         T max = (T) first.item;
         
         return maxR(first,max,comparator);
    }
    
    public T maxR(Node curr,T max,LinkListComparator<T> comparator)
    {
        if(curr == null)
            return max;
        
        if(comparator.compare((T)curr.item, max))
            max = (T)curr.item;
                
        return maxR(curr.next,max,comparator);
    }
    
    public void remove(T item,LinkListComparator<T> comparator)
    {
        Node prev = first;
        Node curr = first;
        
        while(curr != null)
        {
            if(comparator.compare((T)curr.item, item))
            {
                if(curr == first)
                {
                    first = curr.next;
                    
                    curr.next = null;
                    
                    prev  = first;
                    curr  = first;
                                       
                    continue;
                }
                else
                {
                    prev.next = curr.next;
                    
                    //curr.next = null;
                }
            }
            else
                prev = curr;
            
            curr = curr.next;
        }
    }
    
    public void insertAfter(Node<T> first,Node<T> second,LinkListComparator<T> comparator)
    {
        Node<T> curr = this.first;
        
        while(curr != null)
        {
            if(comparator.compare(first.item, curr.item))
            {
                second.next  = curr.next;
                curr.next   = second;
                
                ++size;
                
                break;
            }
             
            curr = curr.next;
        }
    }
    
    public boolean find(T item,LinkListComparator comparator)
    {
        Node curr = first;
        
        while(curr.next != null)
        {
            //If the item found using the callback method then return true
            if(comparator.compare(item, curr.item))
                return true;
            
            curr = curr.next;
        }
        
        return false;
    }
    
    public void delete(int position)
    {
        int  index = 0;
        Node prev  = first;
        Node curr  = first;
        
        //Handling zero index deletion since both prev and curr 
        //points to the same first node
        if(position == 0)
        {
            first = first.next;
            return;
        }
        
        while(curr.next != null && index < size)
        {
            if(index == position)
                break;
            
            prev = curr;
            curr = curr.next;
            
            ++index;
        }
        
        if(index == position)
        {
            prev.next = curr.next;
        }
        
        --size;
    }
    
    public void add(T item)
    {
        if(first == null)
        {
            first = new Node();  
            
            last = first;            
            first.item = item;
        }
        else
        {
            last.next = new Node();  
            
            last = last.next;            
            last.item = item;
        }
        
        ++size;
    }
    
    @Override
    public Iterator<T> iterator()
    {
        return new ListIterator();
    }
    
    private class ListIterator implements Iterator<T>
    {
       private Node current;
       
       public ListIterator()
       {
           current = first;
       }
       
       @Override
       public boolean hasNext()
       {
           return current != null;
       }
       
       @Override
       public void remove()
       {
       
       }
       
       @Override
       public T next()
       {
           T item = (T) current.item;
           
           current = current.next;
           
           return item;
       }
    }
}
