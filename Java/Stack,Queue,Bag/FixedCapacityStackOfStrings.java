

public class FixedCapacityStackOfStrings
{
    private String []a;
    private int N;
    private int capacity;
    
    public FixedCapacityStackOfStrings(int cap)
    {
        a = new String[cap];
        capacity = cap;
    }
    
    public boolean isEmpty()
    {
        return N == 0;
    }
    
    public int size()
    {
        return N;
    }
    
    public void push(String item)
    {
        a[N++] = item;
    }
    
    public boolean isFull()
    {
        return ( N == capacity );
    }
    
    public String pop()
    {
        return a[--N];
    }
    
    public static void main(String[] args)
    {
        FixedCapacityStackOfStrings s;
        
        s = new FixedCapacityStackOfStrings(100);
        
        while(!StdIn.isEmpty())
        {
            String item = StdIn.readString();
            
            if(!item.equals("-"))
                s.push(item);
            else if(!s.isEmpty())
                StdOut.print(s.pop() + " ");
        }
        
        StdOut.println("(" + s.size() + " left on stack)");
    }
}
