
public class CircularQueue <T>
{
    private Node last;
    
    public CircularQueue()
    {
        last = null;
    }
    
    public void enqueue(T item)
    {  
        Node node = new Node();
        node.item = item;
        
        if(last == null)
        {
           last = node;
        }
        
        node.next = last.next;
        last.next = node;
    }
    
    public T dequeue()
    {
        T item = (T)last.next.item;
        
        if(last.next == last)
            last = null;
        else
            last.next = last.next.next;
        
        return item;
    }
    
    public static void main(String[] args)
    {
        CircularQueue<Integer> circularQueue = new CircularQueue<>();
        
        circularQueue.enqueue(1);
        circularQueue.enqueue(2);
        circularQueue.enqueue(3);
        
        System.out.println(circularQueue.dequeue());
        System.out.println(circularQueue.dequeue());
        System.out.println(circularQueue.dequeue());
    }
}
