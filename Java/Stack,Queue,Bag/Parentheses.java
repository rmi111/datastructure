/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chapter_2;

/**
 *
 * @author romipc
 */
public class Parentheses
{
    public static void main(String[] args)
    {
        boolean isClosed = true;
        
        Stack<Character> stack = new Stack();
        
        while( !StdIn.isEmpty() )
        {
            char ch = StdIn.readChar();
         
            if(ch == '-')
                break;
            
            if(ch == '(' || ch == '[' || ch == '{')
                stack.push(ch);
            else
            {
                if(!isClosed)
                    continue;
                
                char p = stack.pop();
                
                if(ch == ')')
                    isClosed = (p == '(');
                if(ch == '}')
                    isClosed = (p == '{');
                 if(ch == ']')
                    isClosed = (p == '[');
            }
        }
        
        StdOut.println(isClosed);
    }
}
