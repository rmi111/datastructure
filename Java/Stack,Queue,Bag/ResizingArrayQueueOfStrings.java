

/**
 *
 * @author romipc
 */
public class ResizingArrayQueueOfStrings
{
    private int rear;
    private int front;
    private String[] data;
    
    public ResizingArrayQueueOfStrings(int capacity)
    {
        this.rear  = 0;
        this.front = 0;
        this.data  = new String[capacity];
    }
    
    public void enqueue(String item)
    {
        if(rear == data.length)
            resize(data.length * 2);
        
        data[rear++] = item;
    }
    
    public String dequeue()
    {
        String item = data[front ++];
        
        if(front > 0 && front == (data.length / 4))
            resize(data.length / 2);
        
        return item;
    }
    
    private void resize(int size)
    {
        String[] temp = new String[size];
        
        int index = 0;
        
        System.out.println("front: " + front + " rear:" + rear + " size:" + size + " old size:" + data.length);
        
        for(int i = front ; i < rear ; ++i )
            temp[index ++] = data[i];
        
        rear  = rear - front;
        front = 0;
       
        
        data = temp;
    }
    
    public boolean isEmpty()
    {
        return front == rear;
    }
    
    public static void main(String[] args)
    {
        ResizingArrayQueueOfStrings strQ = new ResizingArrayQueueOfStrings(4);
        
        while(StdIn.hasNextLine())
			strQ.enqueue(StdIn.readLine());
        
        while(!strQ.isEmpty())
            StdOut.println(strQ.dequeue());
    }
}