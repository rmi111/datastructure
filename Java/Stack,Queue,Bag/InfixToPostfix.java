/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chapter_2;

/**
 *
 * @author romipc
 */
public class InfixToPostfix 
{
    public static void main(String[] args)
    {
        Stack<Character> operatorStack = new Stack<>();
        Stack<String> operandStack =  new Stack<>();
        
        while( !StdIn.isEmpty() )
        {
            char ch = StdIn.readChar();
         
            if(ch == '|')
                break;            
            if(ch == '+' || ch == '-' || ch == '*' || ch == '/')
                operatorStack.push(ch);
            else if(ch == ')')
            {                
                Character op1 = operatorStack.pop();
                               
                operandStack.push(op1 + "");
            }
            else if(ch != ' ' && ch != '(')
                operandStack.push(ch + "");
        }
        
        Stack<String> exp = new Stack();
        
        while(!operandStack.isEmpty())
        {
            exp.push(operandStack.pop());
//            StdOut.print(operandStack.pop());
        }
        
        while(!exp.isEmpty())
        {
            StdOut.print(exp.pop());
        }
    }
    
    
}
