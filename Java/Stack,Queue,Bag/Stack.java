/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author romipc
 */
public class Stack<Item>
{
    private Node first;
    private int N;
    
    private class Node
    {
        Item item;
        Node next;
    }    
    
    public boolean isEmpty() 
    {
        return first == null; 
    }
    
    public int size()
    {
        return N;
    }
    
    public void push(Item item)
    {
        Node oldfirst = first;
        
        first = new Node();
        first.item = item;
        first.next = oldfirst;
    }
    
    public Item pop()
    {
        Item item = first.item;
        first = first.next;
        N--;        
        return item;
    }
    
    public Item peek()
    {
        return first.item;
    }
    
    public static void main(String[] args)
    {
        Stack<String> s;
        
        s = new Stack<String>();
        
        while(!StdIn.isEmpty())
        {
            String item = StdIn.readString();
            
            if(!item.equals("-"))
                s.push(item);
            else if(!s.isEmpty())
                StdOut.print(s.pop() + " ");
        }
        
        StdOut.println("(" + s.size() + " left on stack)");
    }
}
