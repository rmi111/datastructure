/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author romipc
 */
public class EvaluatePostFix {
     public static void main(String[] args)
    {
        Stack<Integer> operandStack =  new Stack<>();
        
        while( !StdIn.isEmpty() )
        {
            char ch = StdIn.readChar();
         
            if(ch == '|')
                break;            
            if(ch == '+' || ch == '-' || ch == '*' || ch == '/')
            {
                switch(ch)
                {
                    case '+':
                        int val1 = operandStack.pop();
                        int val2 = operandStack.pop();
                        
                        operandStack.push(val1 + val2);
                        break;
                    case '-':
                        int val3 = operandStack.pop();
                        int val4 = operandStack.pop();

                        operandStack.push(val3 - val4);
                    break;
                    case '*':
                        int val5 = operandStack.pop();
                        int val6 = operandStack.pop();

                        operandStack.push(val5 * val6);
                    break;
                    case '/':
                        int val7 = operandStack.pop();
                        int val8 = operandStack.pop();

                        operandStack.push(val7 / val8);
                    break;
                }
            }            
            else
            {
                if(ch != ' ')
                    operandStack.push(Integer.parseInt(ch + ""));
            }
        }
        
        StdOut.print(operandStack.pop());
    }
}
